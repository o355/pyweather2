# PyWeather 2
# (c) 2021  Owen McGinley
# Licensed under the MIT License

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


from inky import InkyWHAT
import busio
import board
import adafruit_shtc3
from PIL import Image, ImageDraw, ImageFont
import PIL.ImageOps
from datetime import datetime
from dateutil import tz
import time
import requests
import sys
import json
import boto3
from configparser import ConfigParser

config = ConfigParser()
config.read("config.ini")

access_key_aws = config.get("AWS", "accesskey")
secret_key_aws = config.get("AWS", "secretkey")
region_aws = config.get("AWS", "region")
purgetime_aws = config.getint("AWS", "purgetime")
tablename_aws = config.get("AWS", "dbname")

aws_enabled = config.getboolean("AWS", "enabled")
if aws_enabled:
    session = boto3.Session(aws_access_key_id=access_key_aws, aws_secret_access_key=secret_key_aws, region_name=region_aws)
    db = session.resource('dynamodb')
    table = db.Table(tablename_aws)

latitude_ds = config.get("DARKSKY", "latitude")
longitude_ds = config.get("DARKSKY", "longitude")
key_ds = config.get("DARKSKY", "key")

flipped_display = config.getboolean("DISPLAY", "flipped")

# Dark Sky weather reading function dodad
with open("dscache.json") as f:
    data = json.load(f)

try:
    ds_conditions = data['ds_conditions']
except KeyError:
    ds_conditions = ""
    
try:
    ds_conditions_icon = data['ds_conditions_icon']
except KeyError:
    ds_conditions_icon = ""
    
try:
    ds_minutely_summary = data['ds_minutely_summary']
except KeyError:
    ds_minutely_summary = ""
    
try:
    ds_minutely_icon = data['ds_minutely_icon']
except KeyError:
    ds_minutely_icon = "cloudy"
    
try:
    ds_timezone = data['ds_timezone']
except KeyError:
    ds_timezone = ""
    
ds_timezone_data = tz.gettz(ds_timezone)
    
try:
    ds_alertvis = data['ds_alertvis']
except KeyError:
    ds_alertvis = False
    

try:
    ds_alertname = data['ds_alertname']
except KeyError:
    ds_alertname = ""
    
try:
    ds_starttime = data['ds_starttime']
except KeyError:
    ds_starttime = 999999999999999
    
try:
    ds_endtime = data['ds_endtime']
except KeyError:
    ds_endtime = 99999999999999999

try:
    ds_temp = data['ds_temp']
except KeyError:
    print("Fail load.")
    ds_temp = "0"
    
try:
    ds_feelslike = data['ds_feelslike']
except KeyError:
    ds_feelslike = "0"
    
try:
    ds_hum = data['ds_hum']
except KeyError:
    ds_hum = "0"
    
try:
    ds_windspeed = data['ds_windspeed']
except KeyError:
    ds_windspeed = "0"
    
try:
    ds_windgust = data['ds_windgust']
except KeyError:
    ds_windgust = "0"
    
try:
    ds_windbear = data['ds_windbear']
except KeyError:
    ds_windbear = ""
    
try:
    ds_hourly = data['ds_hourly']
except KeyError:
    ds_hourly = {"data": []}

try:
    ds_rawtemp = data['ds_rawtemp']
except KeyError:
    ds_rawtemp = 999999999
    
try:
    ds_rawhum = data['ds_rawhum']
except KeyError:
    ds_rawhum = 9999999
try:
    ds_daily = data['ds_daily']
except KeyError:
    ds_daily = {"data": []}
    
try:
    ds_lastupdated = data['ds_lastupdated']
except KeyError:
    ds_lastupdated = 0
    
try:
    ds_update_intv = data['ds_update_intv']
except KeyError:
    ds_update_intv = 900
    
try:
    ds_lastupdated_text = data['ds_lastupdated_text']
except KeyError:
    ds_lastupdated_text = "Last updated: 12:00 AM"
    
try:
    ds_sunrise_curday = data['ds_sunrise_curday']
except KeyError:
    ds_sunrise_curday = 0
    
try:
    ds_sunset_curday = data['ds_sunset_curday']
except KeyError:
    ds_sunset_curday = 0
    
try:
    ds_sunrise_nextday = data['ds_sunrise_nextday']
except KeyError:
    ds_sunrise_nextday = 0
    
try:
    ds_sunset_nextday = data['ds_sunset_nextday']
except KeyError:
    ds_sunset_nextday = 0
    

def getwindbearing(bear):
    if bear <= 11.25:
        return "N"
    elif bear <= 33.75:
        return "NNE"
    elif bear <= 56.25:
        return "NE"
    elif bear <= 78.75:
        return "ENE"
    elif bear <= 101.25:
        return "E"
    elif bear <= 123.75:
        return "ESE"
    elif bear <= 146.25:
        return "SE"
    elif bear <= 168.75:
        return "SSE"
    elif bear <= 191.25:
        return "S"
    elif bear <= 213.75:
        return "SSW"
    elif bear <= 236.25:
        return "SW"
    elif bear <= 258.75:
        return "WSW"
    elif bear <= 281.25:
        return "W"
    elif bear <= 303.75:
        return "WNW"
    elif bear <= 326.25:
        return "NW"
    elif bear <= 348.75:
        return "NNW"
    else:
        return "N"


def darkskyget():
    global ds_conditions, ds_conditions_icon, ds_temp, ds_hum, ds_windspeed, ds_windgust
    global ds_windbear, ds_hourly, ds_lastupdated, ds_lastupdated_text, ds_feelslike
    global ds_alertname, ds_alertvis, ds_starttime, ds_endtime, ds_timezone, ds_timezone_data
    global ds_minutely_icon, ds_minutely_summary, ds_rawtemp, ds_rawhum, ds_daily
    global ds_sunrise_curday, ds_sunset_curday, ds_sunrise_nextday, ds_sunset_nextday
    dsget = requests.get("https://api.darksky.net/forecast/%s/%s,%s" % (key_ds, latitude_ds, longitude_ds))
    data = dsget.json()
    ds_conditions = data['currently']['summary']
    if ds_conditions.find("Humid and") != -1:
        ds_conditions = ds_conditions.replace("Humid and ", "")
    
    if ds_conditions.lower().find("snow") != -1:
        ds_conditions = ds_conditions + "!"
        
    try:
        ds_alertname = data['alerts'][0]['title']
        ds_alertvis = True
        ds_endtime = data['alerts'][0]['expires']
        ds_starttime = data['alerts'][0]['time']
        if ds_endtime <= ds_starttime:
            ds_starttime = int(time.time())
    except KeyError:
        ds_alertname = ""
        ds_alertvis = False
        ds_endtime = 99999999999999999
        ds_starttime = 99999999999999999
    
    ds_conditions = ds_conditions.split(" and")[0]
    ds_conditions_icon = data['currently']['icon']
    ds_rawtemp = data['currently']['temperature']
    ds_rawhum = data['currently']['humidity']
    ds_temp = str(int(round(data['currently']['temperature'], 0)))
    ds_feelslike = str(int(round(data['currently']['apparentTemperature'], 0)))
    ds_hum = str(int(round(data['currently']['humidity'] * 100, 0)))
    ds_windspeed = str(int(round(data['currently']['windSpeed'], 0)))
    ds_windgust = str(int(round(data['currently']['windGust'], 0)))
    ds_windbear = getwindbearing(data['currently']['windBearing'])
    ds_hourly = {"data": []}
    ds_daily = {"data": []}
    ds_timezone = data['timezone']
    ds_timezone_data = tz.gettz(ds_timezone)
    try:
        ds_minutely_icon = data['minutely']['icon']
        ds_minutely_summary = data['minutely']['summary']
    except KeyError:
        ds_minutely_icon = "sunny"
        ds_minutely_summary = "N/A"
    
    try:
        ds_sunrise_curday = data['daily']['data'][0]['sunriseTime']
    except KeyError:
        ds_sunrise_curday = 0

    try:
        ds_sunset_curday = data['daily']['data'][0]['sunsetTime']
    except KeyError:
        ds_sunset_curday = 0

    try:
        ds_sunrise_nextday = data['daily']['data'][1]['sunriseTime']
    except KeyError:
        ds_sunrise_nextday = 0

    try:
        ds_sunset_nextday = data['daily']['data'][1]['sunsetTime']
    except KeyError:
        ds_sunset_nextday = 0
    
    ds_index = 0
    i = 0

    while i < 8:
        hourtime = data['hourly']['data'][ds_index]['time']
        dtobj = datetime.fromtimestamp(hourtime, tz=ds_timezone_data)
        dtstr = dtobj.strftime("%-I %p")
        dticon = data['hourly']['data'][ds_index]['icon']
        dttemp = str(int(round(data['hourly']['data'][ds_index]['temperature'], 0)))
        ds_hourly['data'].append({})
        ds_hourly['data'][i]['temp'] = dttemp
        ds_hourly['data'][i]['icon'] = dticon
        ds_hourly['data'][i]['str'] = dtstr
        ds_hourly['data'][i]['timestamp'] = hourtime
        
        # Check for sunrise/sunset conditions
        if (hourtime <= ds_sunrise_curday) and (ds_sunrise_curday < (hourtime + 3600)):
            i = i + 1
            ds_hourly['data'].append({})
            ds_hourly['data'][i]['temp'] = dttemp
            ds_hourly['data'][i]['icon'] = "sunrise"
            ds_hourly['data'][i]['str'] = datetime.fromtimestamp(ds_sunrise_curday, tz=ds_timezone_data).strftime("%-I:%M")
            ds_hourly['data'][i]['timestamp'] = ds_sunrise_curday
        elif (hourtime <= ds_sunset_curday) and (ds_sunset_curday < (hourtime + 3600)):
            i = i + 1
            ds_hourly['data'].append({})
            ds_hourly['data'][i]['temp'] = dttemp
            ds_hourly['data'][i]['icon'] = "sunset"
            ds_hourly['data'][i]['str'] = datetime.fromtimestamp(ds_sunset_curday, tz=ds_timezone_data).strftime("%-I:%M")
            ds_hourly['data'][i]['timestamp'] = ds_sunset_curday
        elif (hourtime <= ds_sunrise_nextday) and (ds_sunrise_nextday < (hourtime + 3600)):
            i = i + 1
            ds_hourly['data'].append({})
            ds_hourly['data'][i]['temp'] = dttemp
            ds_hourly['data'][i]['icon'] = "sunrise"
            ds_hourly['data'][i]['str'] = datetime.fromtimestamp(ds_sunrise_nextday, tz=ds_timezone_data).strftime("%-I:%M")
            ds_hourly['data'][i]['timestamp'] = ds_sunrise_nextday
        elif (hourtime <= ds_sunset_nextday) and (ds_sunset_nextday < (hourtime + 3600)):
            i = i + 1
            ds_hourly['data'].append({})
            ds_hourly['data'][i]['temp'] = dttemp
            ds_hourly['data'][i]['icon'] = "sunset"
            ds_hourly['data'][i]['str'] = datetime.fromtimestamp(ds_sunset_nextday, tz=ds_timezone_data).strftime("%-I:%M")
            ds_hourly['data'][i]['timestamp'] = ds_sunset_nextday
            
        ds_index = ds_index + 1
        i = i + 1
        
    for j in range(0, 4):
        hourtime = data['daily']['data'][j]['time']
        dtobj = datetime.fromtimestamp(hourtime, tz=ds_timezone_data)
        dtstr = dtobj.strftime("%a")
        dticon = data['daily']['data'][j]['icon']
        dttemph = str(int(round(data['daily']['data'][j]['temperatureHigh'], 0)))
        dttempl = str(int(round(data['daily']['data'][j]['temperatureLow'], 0)))
        ds_daily['data'].append({})
        ds_daily['data'][j]['temph'] = dttemph
        ds_daily['data'][j]['templ'] = dttempl
        ds_daily['data'][j]['icon'] = dticon
        ds_daily['data'][j]['str'] = dtstr
    
    ds_lastupdated = time.time()
    currenttime = datetime.now(tz=ds_timezone_data)
    ds_lastupdated_text = "Last updated: " + currenttime.strftime("%-I:%M %p")


i2c = busio.I2C(board.SCL, board.SDA)
sht = adafruit_shtc3.SHTC3(i2c)
inkywhat = InkyWHAT('red')

palette = Image.new('P', (1, 1))
palette.putpalette(
    [
        255, 255, 255,
        0, 0, 0,
        255, 0, 0,
        ] + [0, 0, 0] * 253)


def returnicon(filename):
    icon = Image.open(filename)
    r,g,b,a = icon.split()
    rgb_image = Image.merge("RGB", (r, g, b))
    return PIL.ImageOps.invert(rgb_image)


inkywhat.set_border(inkywhat.BLACK)
diddsupdate = False

if "dsupdate" in sys.argv:
    diddsupdate = True
    darkskyget()

# Forced 15 minute cached run
#ds_lastupdated_delta = time.time() - ds_lastupdated
#if ds_lastupdated_delta >= 900:
#    try:
#        darkskyget()
#    except:
#        pass

alert_active = False
if (ds_endtime >= time.time() and time.time() >= ds_starttime) or ((time.time() + 21600) >= ds_starttime):
    alert_active = True
raw_temperature = (sht.temperature * (9/5)) + 32
raw_humidity = sht.relative_humidity
temperature = str(round(raw_temperature, 1))
humidity = str(int(round(raw_humidity, 0)))
delta_t =  raw_temperature - ds_rawtemp
delta_rh = raw_humidity - (ds_rawhum * 100)

if aws_enabled:
    table.put_item(
        Item={
            'timestamp': int(time.time() // 60 * 60),
            'insidetemp': repr(raw_temperature),
            'insidehum': repr(raw_humidity),
            'deltat': repr(delta_t),
            'deltarh': repr(delta_rh)
        }
    )

    try:
        table.delete_item(
            Key={
                'timestamp': int((time.time() // 60 * 60) - purgetime_aws)
            }
        )
    except:
        pass


def windstr():
    global ds_windgust, ds_windspeed, ds_windbear
    if int(ds_windgust) < 25:
        return "Wind: %s mph %s" % (ds_windspeed, ds_windbear)
    else:
        return "Wind: %s G %s mph" % (ds_windspeed, ds_windgust)


currenttime = datetime.now(tz=ds_timezone_data)
parsed_time = currenttime.strftime("%-I:%M %p")
out = Image.new("RGB", (400, 300), (255, 255, 255))
fnt = ImageFont.truetype("DejaVuSans.ttf", 16)
fnt24 = ImageFont.truetype("DejaVuSans.ttf", 24)
d = ImageDraw.Draw(out)
text_w, text_h = d.textsize(parsed_time, fnt)
curicon = returnicon(ds_conditions_icon + ".png")

# This is where most of the actual display magic happens. It's a mess.
out.paste(curicon, (5, 10))
if (ds_minutely_icon == "rain" or ds_minutely_icon == "snow" or ds_minutely_icon == "sleet"
        or ds_minutely_icon == "hail" or ds_minutely_icon == "thunderstorm" or ds_minutely_icon == "tornado"):
    if ds_minutely_summary.find(", ") != -1:
        d.text((55, 1), ds_temp + "°F", font=fnt24, fill=(0, 0, 0))
        d.text((55, 26), ds_conditions, font=fnt, fill=(0, 0, 0))
        if ds_alertvis is True and alert_active:
            if (int(ds_windspeed) >= 15 or int(ds_windgust) >= 25) and int(ds_feelslike) > 10:
                d.text((235, 4), windstr(), font=fnt, fill=(0, 0, 0))
            else:
                d.text((235, 4), "Feels Like: %s°F" % ds_feelslike, font=fnt, fill=(0, 0, 0))
            d.text((235, 23), "Updated: %s" % parsed_time, font=fnt, fill=(0, 0, 0))
        else:
            d.text((235, 4), "Feels Like: %s°F" % ds_feelslike, font=fnt, fill=(0, 0, 0))
            d.text((235, 23), windstr(), font=fnt, fill=(0, 0, 0))
        d.text((55, 45), ds_minutely_summary.split(", ")[0] + ",", font=fnt, fill=(0, 0, 0))
        d.text((55, 64), ds_minutely_summary.split(", ")[1], font=fnt, fill=(0, 0, 0))
    else:
        d.text((55, 8), ds_temp + "°F", font=fnt24, fill=(0, 0, 0))
        d.text((55, 33), ds_conditions, font=fnt, fill=(0, 0, 0))
        if ds_alertvis is True and alert_active:
            if (int(ds_windspeed) >= 15 or int(ds_windgust) >= 25) and int(ds_feelslike) > 10:
                d.text((235, 11), windstr(), font=fnt, fill=(0, 0, 0))
            else:
                d.text((235, 11), "Feels Like: %s°F" % ds_feelslike, font=fnt, fill=(0, 0, 0))
            d.text((235, 30), "Updated: %s" % parsed_time, font=fnt, fill=(0, 0, 0))
        else:
            d.text((235, 11), "Feels Like: %s°F" % ds_feelslike, font=fnt, fill=(0, 0, 0))
            d.text((235, 30), windstr(), font=fnt, fill=(0, 0, 0))
        d.text((55, 52), ds_minutely_summary, font=fnt, fill=(0, 0, 0))
else:
    d.text((55, 20), ds_temp + "°F", font=fnt24, fill=(0, 0, 0))
    d.text((55, 45), ds_conditions, font=fnt, fill=(0, 0, 0))
    d.text((235, 14), "Feels Like: %s°F" % ds_feelslike, font=fnt, fill=(0, 0, 0))
    d.text((235, 33), windstr(), font=fnt, fill=(0, 0, 0))
    if ds_alertvis is True and alert_active:
        d.text((235, 52), "Updated: %s" % parsed_time, font=fnt, fill=(0, 0, 0))
    else:
        d.text((235, 52), "Humidity: %s%%" % ds_hum, font=fnt, fill=(0, 0, 0))
d.line((0, 85, 400, 85), fill=0, width=2)
d.line((0, 177, 400, 177), fill=0, width=2)
d.line((0, 269, 400, 269), fill=0, width=2)
d.line((80, 85, 80, 177), fill=0, width=2)
d.line((160, 85, 160, 177), fill=0, width=2)
d.line((240, 85, 240, 177), fill=0, width=2)
d.line((320, 85, 320, 177), fill=0, width=2)
d.line((100, 177, 100, 269), fill=0, width=2)
d.line((200, 177, 200, 269), fill=0, width=2)
d.line((300, 177, 300, 269), fill=0, width=2)
start_x_hicon = 16
start_x_hicon_daily = 26
current_time = int(time.time()) + 60
slots_filled = 0
hourly_index = 0
while slots_filled < 5:
    if ds_hourly['data'][hourly_index]['timestamp'] < current_time:
        hourly_index = hourly_index + 1
        continue
    
    h0icon = returnicon(ds_hourly['data'][hourly_index]['icon'] + ".png")
    out.paste(h0icon, (start_x_hicon + (slots_filled * 80), 107))
    text_w, text_h = d.textsize(ds_hourly['data'][hourly_index]['str'], fnt)
    d.text((((80 - text_w) / 2) + (slots_filled * 80), 90), ds_hourly['data'][hourly_index]['str'], font=fnt, fill=(0, 0, 0))
    text_w, text_h = d.textsize(ds_hourly['data'][hourly_index]['temp'] + "°F", fnt)
    d.text(((((80 - text_w) / 2) + (slots_filled * 80)), 155), ds_hourly['data'][hourly_index]['temp'] + "°F", font=fnt, fill=(0, 0, 0))
    slots_filled = slots_filled + 1
    hourly_index = hourly_index + 1

for i in range(0, 4):
    h0icon = returnicon(ds_daily['data'][i]['icon'] + ".png")
    out.paste(h0icon, (start_x_hicon_daily + (i * 100), 200))
    text_w, text_h = d.textsize(ds_daily['data'][i]['str'], fnt)
    d.text((((100 - text_w) / 2) + (i * 100), 182), ds_daily['data'][i]['str'], font=fnt, fill=(0, 0, 0))
    text_w, text_h = d.textsize(ds_daily['data'][i]['temph'] + " | " + ds_daily['data'][i]['templ'], fnt)
    d.text(((((100 - text_w) / 2) + (i * 100)), 247), ds_daily['data'][i]['temph'] + " | " + ds_daily['data'][i]['templ'], font=fnt, fill=(0, 0, 0))

if ds_alertvis and alert_active:
    if ds_endtime >= time.time() >= ds_starttime:
        alerttime = datetime.fromtimestamp(ds_endtime, tz=ds_timezone_data)
        alert_text = ds_alertname + " until " + alerttime.strftime("%-m/%-d %-I:%M %p") + "."
        text_w, text_h = d.textsize(alert_text, fnt)
        d.text(((400 - text_w) / 2, 277), alert_text, font=fnt, fill=(0, 0, 0))
    else:
        alerttime = datetime.fromtimestamp(ds_starttime, tz=ds_timezone_data)
        alert_text = ds_alertname + " starts at " + alerttime.strftime("%-I:%M %p") + "."
        text_w, text_h = d.textsize(alert_text, fnt)
        d.text(((400 - text_w) / 2, 277), alert_text, font=fnt, fill=(0, 0, 0))
else:
    d.line((200, 269, 200, 300), fill=0, width=2)
    temp_text = "Inside: %s°F, %s%% RH" % (temperature, humidity)
    text_w, text_h = d.textsize(temp_text, fnt)
    d.text(((200 - text_w) / 2, 277), temp_text, font=fnt, fill=(0, 0, 0))
    text_w, text_h = d.textsize("Updated: " + parsed_time, fnt)
    d.text((((200 - text_w) / 2) + 200, 277), "Updated: " + parsed_time, font=fnt, fill=(0, 0, 0))

out = out.quantize(colors=3, palette=palette)
if flipped_display:
    out = PIL.ImageOps.flip(out)
    out = PIL.ImageOps.mirror(out)
inkywhat.set_image(out)
inkywhat.show(busy_wait=False)

output_cache = {
    "ds_conditions": ds_conditions,
    "ds_conditions_icon": ds_conditions_icon,
    "ds_sunrise_curday": ds_sunrise_curday,
    "ds_sunset_curday": ds_sunset_curday,
    "ds_sunrise_nextday": ds_sunrise_nextday,
    "ds_sunset_nextday": ds_sunset_nextday,
    "ds_alertvis": ds_alertvis,
    "ds_timezone": ds_timezone,
    "ds_alertname": ds_alertname,
    "ds_starttime": ds_starttime,
    "ds_endtime": ds_endtime,
    "ds_rawtemp": ds_rawtemp,
    "ds_rawhum": ds_rawhum,
    "ds_temp": ds_temp,
    "ds_minutely_icon": ds_minutely_icon,
    "ds_minutely_summary": ds_minutely_summary,
    "ds_feelslike": ds_feelslike,
    "ds_hum": ds_hum,
    "ds_windspeed": ds_windspeed,
    "ds_windgust": ds_windgust,
    "ds_windbear": ds_windbear,
    "ds_hourly": ds_hourly,
    "ds_daily": ds_daily,
    "ds_lastupdated": ds_lastupdated,
    "ds_update_intv": ds_update_intv,
    "ds_lastupdated_text": ds_lastupdated_text
}

with open("dscache.json", "w") as json_file:
    json.dump(output_cache, json_file)
