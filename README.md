# PyWeather 2
Weather display software for the Pimoroni Inky What + SHTC3

# Spaghet-o-meter
As a prototype, PyWeather 2 has some nasty spaghetti code. Before upload, I put some stuff in a config file just so things were less worse. A 9/10 for sure.

# Image of PyWeather 2
(TBD, need to figure out how to do this!)

# Hardware Setup
PyWeather 2 is not so modular, so you're going to need certain hardware to make it work. The list is below.
* Pimoroni Inky What (400x300 e-ink display @ 4.2 inches. Theoretically this will work on any 400x300 e-ink display).
* SHTC3 temperature/humidity sensor connected via I2C
* A Raspberry Pi (For PW2, I was running on a 3B/3B+), or equivalent SBC.

Total parts cost is about $100.

## A few notes about hardware
PyWeather 2 is made to work with the black and white variant of the Inky What - but it can work with the color variants too.

The SHTC3 is a good temperature sensor that has solid accuracy at a good price ($6.95). Of course, you can modify the code to work with other temperature sensors.

# Software Setup
To get started with PyWeather 2, you'll need:
* Python 3.5 or higher
* The requests, boto3, and Pillow libraries
* Standard I2C Libraries for Python 3
* I2C enabled on your device
* The SHTC3 Adafruit CircuitPython library
* A Dark Sky API key. Here's the biggest issue with this project. You can definitely get the code to work with other APIs with enough modification, or use a drop-in Dark Sky API replacement.

Once you have the software & hardware set up, 

# Configuration Setup
PyWeather 2 has a configuration file so you can configure various settings, under the file `config.ini`.

## DARKSKY section
The `key` value is where you'll enter your Dark Sky API key.

The `latitude` and `longitude` value is where you'll enter the latitude and longitude for where the weather display will grab data for.

Relatively self explanatory stuff.

## AWS section
This deals with setting up AWS DynamoDB uploads for the How Hot Is It In My Dorm Room? portal to work. This entire section can be enabled/disabled via the `enabled` value. Set this to False if you don't want set this up.

`accesskey` and `secretkey` are where you should put your access key and secret key, which both have access to the DynamoDB database that you'll be using for How Hot.

`region` is the region code for where the DynamoDB database is being stored (e.g. `us-east-2`).

`dbname` is the name of the DynamoDB database where data should be stored.

### Database configuration
This is how you should be configuring your DynamoDB database:
* Primary key: `timestamp` as `Number`.
* No sort key
* 1 read/write capacity unit should work good. Increase your read capacity units if you expect lots of traffic on your portal. 

`purgetime` is the time, in seconds, that data can remain in the database. This is `259200` (72 hours) by default.

### Note about purging
PyWeather 2 ONLY does point-in-time deletion, rather than retroactive deletion. If PyWeather 2 fails to delete a datapoint as it should, you'll have lingering data in your database that you'll need to manually delete.

PyWeather 3 fixed this flaw. A PR is welcome if you'd like to fix it in code.

## DISPLAY section
`flipped` determines if the output image is flipped around 180 degrees. This is set to False by default.

Set this to True if you install PyWeather 2 such that the white "bezel" is facing upwards (e.g. the Pi power connector/I2C wires come out of the relative *top* of the display.)

# Cron configuration
PyWeather 2 includes a caching system, where it can run on cached data and not call Dark Sky for a refresh (except if precipitation is in the next-hour forecast). However, during cached runs, AWS data is uploaded.

You can run PyWeather 2 on a cached or uncached setup. Instructions for both via cron are below.

**Note: Caching has been added back to the code, but is untested. **

## Uncached only
A good rate to refresh PyWeather 2 at going totally uncached is 5 minutes (288 requests/day). A cron setup for this would look like:

`0,5,10,15,20,25,30,35,40,45,50,55 * * * * /whereever/the/script/to/run/uncached/pw2/is/startds.sh`

## Cached only
A good rate to refresh PyWeather 2 going cached is at 15 minutes for uncached, 5 minutes for cached. Set this up in cron via these two lines.

`0,15,30,45 * * * * /whereever/the/script/to/run/cached/pw2/is/startds-cached.sh`

`5,10,20,25,35,40,50,55 * * * * /wherever/the/script/to/run/uncached/pw2/is/startds.sh`

Make sure to modify (and `chmod +x`) startds-cached and startds so that they launch from the directory that PyWeather 2 is located in.

From there, PyWeather 2 should be running. Enjoy!

# License
PyWeather 2 is licensed under the MIT License.

PyWeather 2 includes Climacons. I am redistributing them here as they are specially modified to work with PyWeather 2 on a 400x300 e-ink screen. No creative credit is being claimed.
